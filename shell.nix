with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "rizeppi-backend";
  buildInputs = [
    adoptopenjdk-bin
    gradle
    postgresql
  ];
}
