package com.tjmaynes.rizeppi;

import com.tjmaynes.rizeppi.domain.Recipe;
import com.tjmaynes.rizeppi.models.IdObject;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@FeignClient(name = "recipe-service")
public interface RecipeClient {
    @RequestMapping(method = RequestMethod.POST, value = "/recipes/new", consumes = "application/json")
    IdObject createRecipe(@RequestBody Recipe recipe);

    @GetMapping("/recipes")
    Page<Recipe> findAll(Pageable page);

    @GetMapping( "/recipes?name={name}")
    Optional<Recipe> findByName(@Param("name") String name);

    @DeleteMapping("/recipes/{id}")
    Recipe deleteRecipe(@RequestParam("id") Long id);
}