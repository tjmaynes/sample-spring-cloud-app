package com.tjmaynes.rizeppi.models;

import com.tjmaynes.rizeppi.domain.Recipe;
import lombok.Value;

@Value
public class RecipeResource {
    Recipe recipe;
}
