package com.tjmaynes.rizeppi.models;

import lombok.Value;

@Value
public class IdObject {
    Long id;
}
