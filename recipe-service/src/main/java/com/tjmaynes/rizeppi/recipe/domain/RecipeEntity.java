package com.tjmaynes.rizeppi.recipe.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.Collection;

@Data
class RecipeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String description;

    private Collection<String> ingredients;

    private String cooktime;

    private Collection<String> tags;
}
