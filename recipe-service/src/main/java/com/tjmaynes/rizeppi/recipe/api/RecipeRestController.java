package com.tjmaynes.rizeppi.recipe.api;

import com.tjmaynes.rizeppi.domain.Recipe;
import com.tjmaynes.rizeppi.recipe.domain.RecipeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Collection;

@RestController
@RequestMapping("/recipes")
@RequiredArgsConstructor
public class RecipeRestController {
    private final RecipeService service;

    @GetMapping("/{id}")
    public Mono<Recipe> getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/")
    public Mono<Collection<Recipe>> getAll() {
        return service.getAllByUserId("some-id");
    }
}
