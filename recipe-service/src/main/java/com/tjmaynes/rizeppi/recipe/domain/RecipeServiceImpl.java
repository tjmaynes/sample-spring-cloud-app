package com.tjmaynes.rizeppi.recipe.domain;

import com.tjmaynes.rizeppi.domain.Recipe;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;

@Service
public class RecipeServiceImpl implements RecipeService {
    @Override
    public Mono<Recipe> getById(Long id) { return Mono.just(null); }

    @Override
    public Mono<Collection<Recipe>> getAllByUserId(String userId) {
        return Mono.just(List.of());
    }
}
