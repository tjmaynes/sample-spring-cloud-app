package com.tjmaynes.rizeppi.recipe.domain;

import com.tjmaynes.rizeppi.domain.Recipe;
import reactor.core.publisher.Mono;

import java.util.Collection;

public interface RecipeService {
    Mono<Recipe> getById(Long id);
    Mono<Collection<Recipe>> getAllByUserId(String userId);
}
