package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description 'Should return one recipe in a list'

    request {
        method GET()
        url '/recipes'
    }
    response {
        status OK()
        body '''\
            [
                {
                    "name": "Mom's Lasagna",
                    "description": "An award-winning recipe for Lasagna (made by my mom)",
                    "cooking_time": "7 minutes",
                    "rating": "4",
                    "ingredients": [
                        {
                            "amount": 1,
                            "size": "1 pound",
                            "name": "mild sausage meat"
                        },
                        {
                            "amount": 1,
                            "size": "1/2 cup",
                            "name": "water"
                        }
                    ],
                    "directions": [
                        "set oven to 420 degrees",
                        "let cool for 10 minutes"
                    ],
                    "serving": "4"
                }
            ]
        '''
        headers {
            contentType('application/json')
        }
    }
}