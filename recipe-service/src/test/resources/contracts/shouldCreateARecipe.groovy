package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description("When a POST request with a Recipe is made, the created recipe's ID is returned")
    request {
        method 'POST'
        url '/recipes/new'

        body('''\
            {
                "name": "Mom's Lasagna",
                "description": "An award-winning recipe for Lasagna (made by my mom)",
                "cooking_time": "7 minutes",
                "rating": "4",
                "ingredients": [
                    {
                        "amount": 1,
                        "size": "1 pound",
                        "name": "mild sausage meat"
                    },
                    {
                        "amount": 1,
                        "size": "1/2 cup",
                        "name": "water"
                    }
                ],
                "directions": [
                    "set oven to 420 degrees",
                    "let cool for 10 minutes"
                ],
                "serving_size": "4"
            }
        ''')
        headers {
            contentType(applicationJson())
        }
    }
    response {
        status 201
        body(id: 42)
        headers {
            contentType(applicationJson())
        }
    }
}