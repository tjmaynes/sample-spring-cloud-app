package com.tjmaynes.rizeppi.recipe.api;

import com.tjmaynes.rizeppi.RecipeApplication;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.web.context.WebApplicationContext;

@AutoConfigureMessageVerifier
@SpringBootTest(classes = {RecipeApplication.class})
public abstract class RecipeControllerBase {
    @Autowired
    private WebApplicationContext webAppContextSetup;

    @BeforeEach
    public void setup() {
        RestAssuredMockMvc.webAppContextSetup(webAppContextSetup);
    }
}
