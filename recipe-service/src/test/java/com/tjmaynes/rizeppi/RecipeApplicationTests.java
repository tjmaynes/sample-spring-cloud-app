package com.tjmaynes.rizeppi;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RecipeApplicationTests {
    @Rule
    private GenericContainer db = new GenericContainer(DockerImageName.parse("postgres:9.5.14-alpine"))
            .withExposedPorts(5432);

	@Test
	void contextLoads() {
    }

    @Test
    void GET_recipes_whenRecipesForAUserExist_itShouldReturnAllRecipesByUserId() {

    }
}
