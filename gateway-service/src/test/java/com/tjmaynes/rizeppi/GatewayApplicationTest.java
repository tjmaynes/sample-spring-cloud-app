package com.tjmaynes.rizeppi;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = { "service.recipeServiceUri=http://localhost:${wiremock.server.port}" }
)
@AutoConfigureStubRunner(
        stubsMode = StubRunnerProperties.StubsMode.LOCAL,
        ids = {"com.tjmaynes.rizeppi:recipe-service:+:stubs:9998"}
)
@AutoConfigureWireMock(port = 0)
public class GatewayApplicationTest {
    @Autowired
    private WebTestClient webClient;

    @Test
    public void GET_recipes_shouldRedirectToRecipesService() {
        webClient
                .get().uri("/api/v1/recipes")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$").isEqualTo("[]");
    }

    @Test
    public void GET_recipe_by_name_shouldRedirectToRecipesService() {
        webClient
                .get().uri("/api/v1/recipes?name=11121")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$").isEqualTo("{}");
    }
}