package com.tjmaynes.rizeppi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties(ServiceUriConfiguration.class)
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public RouteLocator recipeRoutes(RouteLocatorBuilder builder, ServiceUriConfiguration uriConfiguration) {
        return builder.routes()
                .route(p -> p
                        .path("/api/v1/recipes**")
                        .and()
                        .path("/api/v1/recipes/**")
                        .filters(f -> f.rewritePath("/api/v1/recipes", "/recipes"))
                        .uri(uriConfiguration.getRecipeServiceUri()))
                .build();
    }
}