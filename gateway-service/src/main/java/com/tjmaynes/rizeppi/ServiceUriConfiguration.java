package com.tjmaynes.rizeppi;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="service")
public class ServiceUriConfiguration {
    private String recipeServiceUri = "lb://recipe-service";

    public String getRecipeServiceUri() {
        return recipeServiceUri;
    }

    public void setRecipeServiceUri(String uri) {
        this.recipeServiceUri = uri;
    }
}
