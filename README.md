# Rizeppi Backend
[![CI](https://github.com/tjmaynes/rizeppi-backend/actions/workflows/ci.yml/badge.svg)](https://github.com/tjmaynes/rizeppi-backend/actions/workflows/ci.yml)
> A backend for a recipe-sharing application.

## Requirements

- [GNU Make](https://www.gnu.org/software/make)
- [Java](https://https://adoptopenjdk.net/)
- [Docker](https://docs.docker.com/docker-for-mac/install/)

## Usage

To build the services, run the following command:
```bash
make build 
```

To run all tests, run the following command:
```bash
make tests
```

To ship the project, run the following command:
```bash
make ship_it
```

To build a docker image, run the following command:
```bash
make build_image
```

To push a docker image, run the following command:
```bash
make push_image
```
