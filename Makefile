GATEWAY_SERVICE_PORT=8080
RECIPE_SERVICE_PORT=5000
RECIPE_DB_URL="r2dbc:postgresql://postgres:password@localhost:5432/rizeppi?sslmode=disable"

build:
	gradle ":$(SERVICE_NAME):build" -x test

test:
	./gradlew test

ship_it: fail_for_unstaged_files test
	git push

fail_for_unstaged_files:
	git diff-index --quiet HEAD -- || echo "Have unstaged files!"; exit 1;

build_image:
	./scripts/build-image.sh "$(SERVICE_NAME)" "../Dockerfile"

push_image:
	 ./scripts/push-image.sh
