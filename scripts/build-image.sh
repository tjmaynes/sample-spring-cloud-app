#!/bin/bash

set -e

JAR_NAME=$1
DOCKERFILE_LOCATION=$2

function check_requirements() {
  if [[ -z "$REGISTRY_USERNAME" ]]; then
    echo "Please provide a registry username for the image."
    exit 1
  elif [[ -z "$IMAGE_NAME" ]]; then
    echo "Please provide an image name for the image."
    exit 1
  elif [[ -z "$TAG" ]]; then
    echo "Please provide an tag for the image."
    exit 1
  elif [[ -z "$JAR_NAME" ]]; then
    echo "Please provide a name for the jar you are trying to build via arg 1"
    exit 1
  elif [[ -z "$DOCKERFILE_LOCATION" ]]; then
    echo "Please provide a location for the Dockerfile via arg 2"
    exit 1
  elif [[ ! -f "$DOCKERFILE_LOCATION" ]]; then
    echo "Please provide a real location for Dockerfile: ${DOCKERFILE_LOCATION}"
    exit 1
  fi
}

function build_image() {
  docker build \
    --tag $REGISTRY_USERNAME/$IMAGE_NAME:$TAG \
    --build-arg JAR_NAME=$JAR_NAME \
    $DOCKERFILE_LOCATION
}

function main() {
  check_requirements
  build_image
}

main