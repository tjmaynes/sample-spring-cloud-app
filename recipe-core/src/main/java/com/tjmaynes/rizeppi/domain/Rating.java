package com.tjmaynes.rizeppi.domain;

public enum Rating {
    AMAZING,
    GREAT,
    GOOD,
    OKAY,
    NOT_GOOD,
    DONT_TRY,
    BAD
}
