package com.tjmaynes.rizeppi.domain;

import lombok.Value;

import java.util.Collection;

@Value
public class Recipe {
    String name;
    String description;
    String cookingTime;
    Rating rating;
    Collection<Ingredient> ingredients;
    Collection<String> directions;
    int servingSize;
}
