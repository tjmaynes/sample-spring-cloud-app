package com.tjmaynes.rizeppi.domain;

import lombok.Value;

@Value
public class Ingredient {
    String name;
    String size;
    int amount;
}
